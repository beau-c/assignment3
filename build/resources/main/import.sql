-- Inserting franchises
INSERT INTO tb_franchise (name, description) VALUES ('Mission: Impossible', 'The first installment of the Marvel Cinematic Universe.');
INSERT INTO tb_franchise (name, description) VALUES ('Star Wars', 'Star Wars is an American epic space-opera multimedia franchise created by George Lucas, which began with the eponymous 1977 film and quickly became a worldwide pop-culture phenomenon.');
INSERT INTO tb_franchise (name, description) VALUES ('Batman', 'Since his first appearance in 1939, Batman has been adapted into various media such as film, radio, television, and video games, as well as numerous merchandising items. The Batman franchise has become one of the highest-grossing media franchises of all time.');
INSERT INTO tb_franchise (name, description) VALUES ('Fast & Furious', 'Fast & Furious (also known as The Fast and the Furious) is a media franchise centered on a series of action films that are largely concerned with street racing, heists, spies, and family. The franchise also includes short films, a television series, live shows, video games and theme park attractions. It is distributed by Universal Pictures. The franchise is the second-highest-grossing media franchise of all time, with a total box office collection of $1.5 billion.');
INSERT INTO tb_franchise (name, description) VALUES ('The Lord of the Rings', 'The Lord of the Rings is a media franchise centered on a series of epic fantasy adventure films that began in the late 19th century. The films are set in the fictional world of Middle-earth, and are based on the novel The Lord of the Rings by J. R. R. Tolkien. The franchise is distributed by Warner Bros. Pictures. The franchise is the third-highest-grossing media franchise of all time, with a total box office collection of $1.5 billion.');
INSERT INTO tb_franchise (name, description) VALUES ('The Avengers', 'The Avengers is a media franchise centered on a series of action films that began in 2012. The films are set in the fictional world of Earth, and are based on the Marvel Comics superhero team of the same name. The franchise is distributed by Marvel Studios. The franchise is the fifth-highest-grossing media franchise of all time, with a total box office collection of $1.5 billion.');

-- Inserting movies
INSERT INTO tb_movie (director, franchise_id, genre, picture_url, release_year, title, trailer_url) VALUES ('Christopher McQuarrie', 1, 'Action', null, 2018, 'Mission: Impossible - Fallout', 'https://www.youtube.com/watch?v=wb49-oV0F78');
INSERT INTO tb_movie (director, franchise_id, genre, picture_url, release_year, title, trailer_url) VALUES ('George Lucas', 2, 'Science Fiction, Adventure', null, 1999, 'Star Wars: Episode I: The Phantom Menace', 'https://www.youtube.com/watch?v=bD7bpG-zDJQ');
INSERT INTO tb_movie (director, franchise_id, genre, picture_url, release_year, title, trailer_url) VALUES ('George Lucas', 2, 'Science Fiction, Adventure', null, 2002, 'Star Wars: Episode II: Attack of the Clones', 'https://www.youtube.com/watch?v=gYbW1F_c9eM');
INSERT INTO tb_movie (director, franchise_id, genre, picture_url, release_year, title, trailer_url) VALUES ('George Lucas', 2, 'Science Fiction, Adventure', null, 2010, 'Star Wars: Episode III: Revenge of the Sith', 'https://www.youtube.com/watch?v=5UfA_aKBGMc');
INSERT INTO tb_movie (director, franchise_id, genre, picture_url, release_year, title, trailer_url) VALUES ('George Lucas', 2, 'Science Fiction, Adventure', null, 2015, 'Star Wars: The Force Awakens', 'https://www.youtube.com/watch?v=sGbxmsDFVnE');
INSERT INTO tb_movie (director, franchise_id, genre, picture_url, release_year, title, trailer_url) VALUES ('George Lucas', 2, 'Science Fiction, Adventure', null, 2005, 'Star Wars: Episode V: The Empire Strikes Back', 'https://www.youtube.com/watch?v=PkqqbFZ7Pjo');
INSERT INTO tb_movie (director, franchise_id, genre, picture_url, release_year, title, trailer_url) VALUES ('George Lucas', 2, 'Science Fiction, Adventure', null, 2004, 'Star Wars: Episode VIII: The Last Jedi', 'https://www.youtube.com/watch?v=Q0CbN8sfihY');
INSERT INTO tb_movie (director, franchise_id, genre, picture_url, release_year, title, trailer_url) VALUES ('George Lucas', 2, 'Science Fiction, Adventure', null, 2007, 'Star Wars: Episode IX: The Rise of Skywalker', 'https://www.youtube.com/watch?v=frdj1zb9sMY');
INSERT INTO tb_movie (director, franchise_id, genre, picture_url, release_year, title, trailer_url) VALUES ('George Lucas', 2, 'Science Fiction, Adventure', null, 2008, 'Star Wars: Episode VI: Return of the Jedi', 'https://www.youtube.com/watch?v=5UfA_aKBGMc');
INSERT INTO tb_movie (director, franchise_id, genre, picture_url, release_year, title, trailer_url) VALUES ('George Lucas', 2, 'Science Fiction, Adventure', null, 2012, 'Star Wars: Episode VII: The Force Awakens', 'https://www.youtube.com/watch?v=sGbxmsDFVnE');
INSERT INTO tb_movie (director, franchise_id, genre, picture_url, release_year, title, trailer_url) VALUES ('Christopher McQuarrie', 3, 'Action', null, 2018, 'Batman: The Dark Knight', 'https://www.youtube.com/watch?v=EXeTwQWrcwY');
INSERT INTO tb_movie (director, franchise_id, genre, picture_url, release_year, title, trailer_url) VALUES ('Peter Jackson', 5, 'Adventure', null, 2001, 'The Lord of the Rings: The Fellowship of the Ring', 'https://www.youtube.com/watch?v=V75dMMIW2B4');
INSERT INTO tb_movie (director, franchise_id, genre, picture_url, release_year, title, trailer_url) VALUES ('Peter Jackson', 5, 'Adventure', null, 2004, 'The Lord of the Rings: The Two Towers', 'https://www.youtube.com/watch?v=V75dMMIW2B4');
INSERT INTO tb_movie (director, franchise_id, genre, picture_url, release_year, title, trailer_url) VALUES ('Peter Jackson', 5, 'Adventure', null, 2012, 'The Lord of the Rings: The Return of the King', 'https://www.youtube.com/watch?v=V75dMMIW2B4');
INSERT INTO tb_movie (director, franchise_id, genre, picture_url, release_year, title, trailer_url) VALUES ('Peter Jackson', 5, 'Adventure', null, 2002, 'The Lord of the Rings: The Two Towers', 'https://www.youtube.com/watch?v=V75dMMIW2B4');
INSERT INTO tb_movie (director, franchise_id, genre, picture_url, release_year, title, trailer_url) VALUES ('Peter Jackson', 5, 'Adventure', null, 2003, 'The Lord of the Rings: The Return of the King', 'https://www.youtube.com/watch?v=V75dMMIW2B4');
INSERT INTO tb_movie (director, franchise_id, genre, picture_url, release_year, title, trailer_url) VALUES ('Peter Jackson', 5, 'Adventure', null, 2005, 'The Lord of the Rings: The Fellowship of the Ring', 'https://www.youtube.com/watch?v=V75dMMIW2B4');

-- Inserting Characters
INSERT INTO tb_character (alias, first_name, gender, last_name, picture_url) VALUES ('The Rock', 'Dwayne', 0, 'Johnson', 'https://m.media-amazon.com/images/M/MV5BMTkyNDQ3NzAxM15BMl5BanBnXkFtZTgwODIwMTQ0NTE@._V1_.jpg');
INSERT INTO tb_character (first_name, gender, last_name, picture_url) VALUES ('Tom', 0, 'Hanks', 'https://m.media-amazon.com/images/M/MV5BMTQ2MjMwNDA3Nl5BMl5BanBnXkFtZTcwMTA2NDY3NQ@@._V1_.jpg');
INSERT INTO tb_character (first_name, gender, last_name, picture_url) VALUES ('Tom', 0, 'Cruise', 'https://m.media-amazon.com/images/M/MV5BMTk1MjM3NTU5M15BMl5BanBnXkFtZTcwMTMyMjAyMg@@._V1_UY1200_CR142,0,630,1200_AL_.jpg');
INSERT INTO tb_character (first_name, gender, last_name, picture_url) VALUES ('Mila', 1, 'Kunis', 'https://m.media-amazon.com/images/M/MV5BODQyNTQyNzY4MV5BMl5BanBnXkFtZTcwODg5MDA3MQ@@._V1_.jpg');

-- Inserting Movie Character relationships
INSERT INTO tb_movie_character (movie_id, character_id) VALUES (1, 1);
INSERT INTO tb_movie_character (movie_id, character_id) VALUES (2, 2);
INSERT INTO tb_movie_character (movie_id, character_id) VALUES (3, 3);
INSERT INTO tb_movie_character (movie_id, character_id) VALUES (4, 4);
INSERT INTO tb_movie_character (movie_id, character_id) VALUES (5, 1);
INSERT INTO tb_movie_character (movie_id, character_id) VALUES (6, 1);
INSERT INTO tb_movie_character (movie_id, character_id) VALUES (7, 2);
INSERT INTO tb_movie_character (movie_id, character_id) VALUES (8, 4);
INSERT INTO tb_movie_character (movie_id, character_id) VALUES (9, 3);
INSERT INTO tb_movie_character (movie_id, character_id) VALUES (10, 1);
INSERT INTO tb_movie_character (movie_id, character_id) VALUES (11, 1);