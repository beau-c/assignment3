# Assignment 3

![Java](https://img.shields.io/badge/java-%23ED8B00.svg?style=for-the-badge&logo=java&logoColor=white)
![Spring](https://img.shields.io/badge/spring-%236DB33F.svg?style=for-the-badge&logo=spring&logoColor=white)
![Swagger](https://img.shields.io/badge/-Swagger-%23Clojure?style=for-the-badge&logo=swagger&logoColor=white)
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

This repository contains the project for assignment 3 of the Noroff Accelerate bootcamp.
It consists of an MVC web application and API using Spring Boot and JPA.

## Description
This application holds CRUD functionality for movies, franchises and characters.
A movie belongs to a franchise and characters appear in movies.
Upon running the application, a PostgreSQL database is generated and populated using mock data found in `/src/main/resources/import.sql`. 

## Running the project
Run `./gradleW bootRun` to build and run the application.

## Usage
View the OpenAPI definition and documentation and also try out the API by navigating to <HOST_URL>/swagger-ui/index.html
