package nl.xvvv.assignment3.mappers.movie;

import nl.xvvv.assignment3.dto.movie.MovieCreateDTO;
import nl.xvvv.assignment3.dto.movie.MovieDTO;
import nl.xvvv.assignment3.models.Character;
import nl.xvvv.assignment3.models.Movie;
import org.springframework.stereotype.Service;

@Service
public class MovieMapperImpl implements MovieMapper {
    @Override
    public MovieDTO toMovieDTO(Movie movie) {
        MovieDTO dto = new MovieDTO();
        dto.setId(movie.getId());
        dto.setTitle(movie.getTitle());
        dto.setGenre(movie.getGenre());
        dto.setDirector(movie.getDirector());
        dto.setReleaseYear(movie.getReleaseYear());
        dto.setPictureURL(movie.getPictureURL());
        dto.setTrailerURL(movie.getTrailerURL());
        for(Character character : movie.getCharacters()) {
            dto.getCharacters().add(character.getId());
        }
        return dto;
    }

    @Override
    public Movie toMovie(MovieDTO dto) {
        Movie movie = new Movie();
        movie.setId(dto.getId());
        movie.setTitle(dto.getTitle());
        movie.setGenre(dto.getGenre());
        movie.setDirector(dto.getDirector());
        movie.setReleaseYear(dto.getReleaseYear());
        movie.setPictureURL(dto.getPictureURL());
        movie.setTrailerURL(dto.getTrailerURL());
        return movie;
    }

    @Override
    public Movie toMovie(MovieCreateDTO dto) {
        Movie movie = new Movie();
        movie.setTitle(dto.getTitle());
        movie.setGenre(dto.getGenre());
        movie.setDirector(dto.getDirector());
        movie.setReleaseYear(dto.getReleaseYear());
        movie.setPictureURL(dto.getPictureURL());
        movie.setTrailerURL(dto.getTrailerURL());
        return movie;
    }
}
