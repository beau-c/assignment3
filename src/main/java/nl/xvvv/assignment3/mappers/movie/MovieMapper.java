package nl.xvvv.assignment3.mappers.movie;

import nl.xvvv.assignment3.dto.movie.MovieCreateDTO;
import nl.xvvv.assignment3.dto.movie.MovieDTO;
import nl.xvvv.assignment3.models.Movie;

public interface MovieMapper {
    /***
     * Maps a {@link Movie} to a {@link MovieDTO}
     * @param movie The {@link Movie} to map
     * @return The {@link MovieDTO}
     */
    MovieDTO toMovieDTO(Movie movie);

    /***
     * Maps a {@link MovieDTO} to a {@link Movie}
     * @param dto The {@link MovieDTO} to map
     * @return The {@link Movie}
     */
    Movie toMovie(MovieDTO dto);

    /***
     * Maps a {@link MovieCreateDTO} to a {@link Movie}
     * @param dto The {@link MovieCreateDTO} to map
     * @return The {@link Movie}
     */
    Movie toMovie(MovieCreateDTO dto);
}
