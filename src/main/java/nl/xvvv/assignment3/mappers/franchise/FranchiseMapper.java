package nl.xvvv.assignment3.mappers.franchise;

import nl.xvvv.assignment3.dto.franchise.FranchiseCreateDTO;
import nl.xvvv.assignment3.dto.franchise.FranchiseDTO;
import nl.xvvv.assignment3.models.Franchise;

public interface FranchiseMapper {
    /***
     * Maps a {@link Franchise} to a {@link FranchiseDTO}
     * @param franchise The {@link Franchise} to map
     * @return The {@link FranchiseDTO}
     */
    FranchiseDTO toFranchiseDTO(nl.xvvv.assignment3.models.Franchise franchise);

    /***
     * Maps a {@link FranchiseCreateDTO} to a {@link Franchise}
     * @param dto The {@link FranchiseCreateDTO} to map
     * @return The {@link Franchise}
     */
    Franchise toFranchise(FranchiseCreateDTO dto);
}
