package nl.xvvv.assignment3.mappers.franchise;

import nl.xvvv.assignment3.dto.franchise.FranchiseCreateDTO;
import nl.xvvv.assignment3.dto.franchise.FranchiseDTO;
import nl.xvvv.assignment3.models.Franchise;
import nl.xvvv.assignment3.models.Movie;
import org.springframework.stereotype.Service;

@Service
public class FranchiseMapperImpl implements FranchiseMapper {

    @Override
    public FranchiseDTO toFranchiseDTO(Franchise franchise) {
        FranchiseDTO dto = new FranchiseDTO();
        dto.setId(franchise.getId());
        dto.setName(franchise.getName());
        dto.setDescription(franchise.getDescription());
        for(Movie movie : franchise.getMovies()) {
            dto.getMovies().add(movie.getId());
        }
        return dto;
    }

    @Override
    public Franchise toFranchise(FranchiseCreateDTO dto) {
        Franchise franchise = new Franchise();
        franchise.setName(dto.getName());
        franchise.setDescription(dto.getDescription());
        return franchise;
    }
}
