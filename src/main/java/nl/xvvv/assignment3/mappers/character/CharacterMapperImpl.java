package nl.xvvv.assignment3.mappers.character;

import nl.xvvv.assignment3.dto.character.CharacterCreateDTO;
import nl.xvvv.assignment3.dto.character.CharacterGetDTO;
import nl.xvvv.assignment3.models.Character;
import nl.xvvv.assignment3.models.Movie;
import org.springframework.stereotype.Service;

@Service
public class CharacterMapperImpl implements CharacterMapper {
    @Override
    public CharacterGetDTO toCharacterDTO(Character character) {
        CharacterGetDTO dto = new CharacterGetDTO();
        dto.setId(character.getId());
        dto.setFullName(character.getFirstName() + " " + character.getLastName());
        dto.setGender(character.getGender().toString());
        dto.setPictureURL(character.getPictureURL());
        dto.setAlias(character.getAlias());
        for(Movie movie : character.getMovies()) {
            dto.getMovies().add(movie.getId());
        }
        return dto;
    }

    @Override
    public Character toCharacter(CharacterCreateDTO dto) {
        Character character = new Character();
        character.setFirstName(dto.getFirstName());
        character.setLastName(dto.getLastName());
        character.setGender(dto.getGender());
        character.setPictureURL(dto.getPictureURL());
        character.setAlias(dto.getAlias());
        return character;
    }
}
