package nl.xvvv.assignment3.mappers.character;

import nl.xvvv.assignment3.dto.character.CharacterCreateDTO;
import nl.xvvv.assignment3.dto.character.CharacterGetDTO;
import nl.xvvv.assignment3.models.Character;

public interface CharacterMapper {
    /***
     * Maps a {@link Character} to a {@link CharacterGetDTO}
     * @param character The {@link Character} to map
     * @return The {@link CharacterGetDTO}
     */
    CharacterGetDTO toCharacterDTO(nl.xvvv.assignment3.models.Character character);

    /***
     * Maps a {@link CharacterCreateDTO} to a {@link Character}
     * @param dto The {@link CharacterCreateDTO} to map
     * @return The {@link Character}
     */
    Character toCharacter(CharacterCreateDTO dto);
}
