package nl.xvvv.assignment3;

import nl.xvvv.assignment3.enums.Gender;
import nl.xvvv.assignment3.models.Character;
import nl.xvvv.assignment3.models.Franchise;
import nl.xvvv.assignment3.models.Movie;
import nl.xvvv.assignment3.repositories.CharacterRepository;
import nl.xvvv.assignment3.repositories.FranchiseRepository;
import nl.xvvv.assignment3.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.Set;

@Component
public class Assignment3Runner implements ApplicationRunner {

    private final CharacterRepository characterRepository;
    private final MovieRepository movieRepository;
    private final FranchiseRepository franchiseRepository;

    @Autowired
    public Assignment3Runner(CharacterRepository characterRepository,
                             MovieRepository movieRepository,
                             FranchiseRepository franchiseRepository) {
        this.characterRepository = characterRepository;
        this.movieRepository = movieRepository;
        this.franchiseRepository = franchiseRepository;
    }

    @Override
    @Transactional
    public void run(ApplicationArguments args) {

//        //System.out.println(characterRepository.findAll());
//
//        Franchise franchiseMI = new Franchise();
//        franchiseMI.setName("Mission: Impossible");
//        franchiseMI.setDescription("The first installment of the Marvel Cinematic Universe.");
//        franchiseMI = franchiseRepository.save(franchiseMI);
//
//        Movie movie = new Movie();
//        movie.setTitle("Mission: Impossible - Fallout");
//        movie.setReleaseYear(2018);
//        movie.setGenre("Action");
//        movie.setDirector("Christopher McQuarrie");
//        movie.setTrailerURL("https://www.youtube.com/watch?v=wb49-oV0F78");
//        movie.setFranchise(franchiseMI);
//        movieRepository.save(movie);
//
//        Character c = new Character();
//        c.setFirstName("Dwayne");
//        c.setLastName("Johnson");
//        c.setAlias("The Rock");
//        c.setGender(Gender.MALE);
//        c.setPictureURL("https://m.media-amazon.com/images/M/MV5BMTkyNDQ3NzAxM15BMl5BanBnXkFtZTgwODIwMTQ0NTE@._V1_.jpg");
//        c.setMovies(Set.of(movie));
//        c = characterRepository.save(c);
//
//        Character c2 = new Character();
//        c2.setFirstName("Tom");
//        c2.setLastName("Cruise");
//        c2.setAlias("Tom Cruise");
//        c2.setGender(Gender.MALE);
//        c2.setPictureURL("https://m.media-amazon.com/images/M/MV5BMTk1MjM3NTU5M15BMl5BanBnXkFtZTcwMTMyMjAyMg@@._V1_UY1200_CR142,0,630,1200_AL_.jpg");
//        c2.setMovies(Set.of(movie));
//        c2 = characterRepository.save(c2);
//
//        Character c3 = new Character();
//        c3.setFirstName("Mila");
//        c3.setLastName("Kunis");
//        c3.setAlias("Mila Kunis");
//        c3.setGender(Gender.FEMALE);
//        c3.setPictureURL("https://m.media-amazon.com/images/M/MV5BODQyNTQyNzY4MV5BMl5BanBnXkFtZTcwODg5MDA3MQ@@._V1_.jpg");
//        c3.setMovies(Set.of(movie));
//        c3 = characterRepository.save(c3);
//
//        Character c4 = new Character();
//        c4.setFirstName("Tom");
//        c4.setLastName("Hanks");
//        c4.setAlias("Tom Hanks");
//        c4.setGender(Gender.MALE);
//        c4.setPictureURL("https://m.media-amazon.com/images/M/MV5BMTQ2MjMwNDA3Nl5BMl5BanBnXkFtZTcwMTA2NDY3NQ@@._V1_.jpg");
//        c4 = characterRepository.save(c4);

        //movie.setFranchise(franchiseMI);
        //movieRepository.save(movie);
    }
}
