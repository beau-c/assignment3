package nl.xvvv.assignment3.repositories;

import nl.xvvv.assignment3.models.Character;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface CharacterRepository extends JpaRepository<Character, Integer> {
    @Query(value = "SELECT * FROM assignment3.public.tb_character WHERE id IN (SELECT character_id FROM assignment3.public.tb_movie_character WHERE movie_id = ?1)", nativeQuery = true)
    Collection<Character> findAllByMovie(Integer id);

    @Query(value = "SELECT * FROM assignment3.public.tb_character WHERE id IN (SELECT character_id FROM assignment3.public.tb_movie_character WHERE movie_id IN (SELECT tb_movie.id FROM assignment3.public.tb_movie WHERE tb_movie.franchise_id = ?1))", nativeQuery = true)
    Collection<Character> findAllByFranchise(Integer id);
}