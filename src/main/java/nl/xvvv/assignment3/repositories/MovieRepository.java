package nl.xvvv.assignment3.repositories;

import nl.xvvv.assignment3.models.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Integer> {
    @Query("SELECT m FROM Movie m WHERE m.franchise.id = ?1")
    Collection<Movie> findAllByFranchise(Integer id);
}
