package nl.xvvv.assignment3.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "tb_franchise")
@Getter
@Setter
public class Franchise {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "franchise_id")
    private int id;

    @Column(name = "name", length = 100, nullable = false)
    private String name;

    @Column(name = "description", length = 500, columnDefinition = "TEXT", nullable = true)
    private String description;

    @OneToMany(mappedBy = "franchise")
    private Set<Movie> movies;
}