package nl.xvvv.assignment3.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "tb_movie")
@Getter
@Setter
public class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "title", length = 100, nullable = false)
    private String title;

    @Column(name = "genre", length = 100, nullable = false)
    private String genre;

    @Column(name = "release_year", nullable = false)
    private int releaseYear;

    @Column(name = "director", length = 100, nullable = false)
    private String director;

    @Column(name = "picture_url", length = 255, nullable = true)
    private String pictureURL;

    @Column(name = "trailer_url", length = 255, nullable = true)
    private String trailerURL;

    @ManyToMany
    @JoinTable(
            name = "tb_movie_character",
            joinColumns = { @JoinColumn(name = "movie_id") },
            inverseJoinColumns = { @JoinColumn(name = "character_id") }
    )
    private Set<Character> characters = new HashSet<>();

    @ManyToOne
    @JoinColumn(name="franchise_id", nullable = false)
    private Franchise franchise;
}