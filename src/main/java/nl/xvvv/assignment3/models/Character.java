package nl.xvvv.assignment3.models;

import lombok.Getter;
import lombok.Setter;
import nl.xvvv.assignment3.enums.Gender;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "tb_character")
@Getter
@Setter
public class Character {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "first_name", length = 64, nullable = false)
    private String firstName;

    @Column(name = "last_name", length = 64, nullable = false)
    private String lastName;

    @Column(name = "alias", length = 64, nullable = true)
    private String alias;

    @Column(name = "gender", nullable = false)
    private Gender gender;

    @Column(name = "picture_url", length = 255, nullable = true)
    private String pictureURL;

    @ManyToMany(mappedBy = "characters")
    private Set<Movie> movies = new HashSet<>();
}