package nl.xvvv.assignment3.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import nl.xvvv.assignment3.dto.movie.MovieCreateDTO;
import nl.xvvv.assignment3.dto.movie.MovieDTO;
import nl.xvvv.assignment3.mappers.movie.MovieMapper;
import nl.xvvv.assignment3.models.Franchise;
import nl.xvvv.assignment3.models.Movie;
import nl.xvvv.assignment3.services.franchise.FranchiseService;
import nl.xvvv.assignment3.services.movie.MovieService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "api/v1/movies")
public class MovieController {

    private final MovieService movieService;
    private final MovieMapper movieMapper;
    private final FranchiseService franchiseService;

    public MovieController(final MovieService movieService, final MovieMapper movieMapper, final FranchiseService franchiseService) {
        this.movieService = movieService;
        this.movieMapper = movieMapper;
        this.franchiseService = franchiseService;
    }

    // GET: /movies/{id}
    @Operation(summary = "Get a movie by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful retrieval of a movie"),
            @ApiResponse(responseCode = "404", description = "Movie not found")
    })
    @GetMapping("/{id}")
    public ResponseEntity<MovieDTO> findById(@PathVariable Integer id) {
        Movie movie = movieService.findById(id);
        if (movie == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(
                movieMapper.toMovieDTO(movie)
        );
    }

    // GET: /movies/
    @Operation(summary = "Get all movies")
    @ApiResponse(responseCode = "200", description = "Successful retrieval of all movies")
    @GetMapping("/")
    public ResponseEntity<Collection<MovieDTO>> findAll() {
        return ResponseEntity.ok(
                movieService.findAll()
                        .stream()
                        .map(movieMapper::toMovieDTO)
                        .collect(Collectors.toList())
        );
    }

    // POST: /movies/
    @Operation(summary = "Create a movie")
    @ApiResponse(responseCode = "201", description = "Successful creation of a movie")
    @PostMapping("/create")
    public ResponseEntity<MovieDTO> create(@RequestBody MovieCreateDTO movieDTO) {
        return new ResponseEntity<>(
                movieMapper.toMovieDTO(
                    movieService.add(
                            movieMapper.toMovie(movieDTO)
                    )
        ), HttpStatus.CREATED);
    }

    // PUT: /movies/{id}
    @Operation(summary = "Update a movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful update of a movie"),
            @ApiResponse(responseCode = "404", description = "Movie not found")
    })
    @PutMapping("/{id}")
    public ResponseEntity<MovieDTO> update(@PathVariable Integer id, @RequestBody MovieCreateDTO movieDTO) {
        if (movieService.findById(id) == null) {
            return ResponseEntity.notFound().build();
        }
        Movie movie = movieMapper.toMovie(movieDTO);
        movie.setId(id);
        return ResponseEntity.ok(
                movieMapper.toMovieDTO(
                        movieService.update(movie)
                )
        );
    }

    // DELETE: /movies/{id}
    @Operation(summary = "Delete a movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful deletion of a movie"),
            @ApiResponse(responseCode = "404", description = "Movie not found")
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<MovieDTO> delete(@PathVariable Integer id) {
        if (movieService.findById(id) == null) {
            return ResponseEntity.notFound().build();
        }
        movieService.deleteById(id);
        return ResponseEntity.ok().build();
    }

    // Update movies in a franchise
    // PUT: /movies/franchise/{franchiseId}
    @Transactional
    @Operation(summary = "Update movies in a franchise")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful update of one or more movies to a franchise"),
            @ApiResponse(responseCode = "404", description = "Franchise or movie not found")
    })
    @PutMapping("/franchise/{franchiseId}")
    public ResponseEntity<Collection<MovieDTO>> updateFranchise(@PathVariable Integer franchiseId, @RequestBody Collection<Integer> movieIds) {
        if (franchiseService.findById(franchiseId) == null) {
            return ResponseEntity.notFound().build();
        }
        Franchise franchise = franchiseService.findById(franchiseId);

        for (Integer movieId : movieIds) {
            Movie movie = movieService.findById(movieId);
            if (movie == null) {
                return ResponseEntity.notFound().build();
            }
            movie.setFranchise(franchise);
            movieService.update(movie);
        }

        return findAllByFranchise(franchiseId);
    }

    // Get all the movies in a franchise
    // GET: /movies/franchise/{franchiseId}
    @Operation(summary = "Get all movies in a franchise")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful retrieval of all movies in a franchise"),
            @ApiResponse(responseCode = "404", description = "Franchise not found")
    })
    @GetMapping("/franchise/{franchiseId}")
    public ResponseEntity<Collection<MovieDTO>> findAllByFranchise(@PathVariable Integer franchiseId) {
        return ResponseEntity.ok(
                movieService.findAllByFranchise(franchiseId)
                        .stream()
                        .map(movieMapper::toMovieDTO)
                        .collect(Collectors.toList())
        );
    }
}
