package nl.xvvv.assignment3.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import nl.xvvv.assignment3.dto.character.CharacterCreateDTO;
import nl.xvvv.assignment3.dto.character.CharacterGetDTO;
import nl.xvvv.assignment3.mappers.character.CharacterMapper;
import nl.xvvv.assignment3.models.Character;
import nl.xvvv.assignment3.services.character.CharacterService;
import nl.xvvv.assignment3.services.movie.MovieService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "api/v1/characters")
public class CharacterController {

    private final CharacterService characterService;
    private final CharacterMapper characterMapper;
    private final MovieService movieService;

    public CharacterController(final CharacterService characterService, final CharacterMapper characterMapper, final MovieService movieService) {
        this.characterService = characterService;
        this.characterMapper = characterMapper;
        this.movieService = movieService;
    }

    // GET: /characters/{id}
    @Operation(summary = "Get a character by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful retrieval of a character"),
            @ApiResponse(responseCode = "404", description = "Character not found")
    })
    @GetMapping("/{id}")
    public ResponseEntity<CharacterGetDTO> findById(@PathVariable Integer id) {
        Character character = characterService.findById(id);
        if (character == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(characterMapper.toCharacterDTO(character));
    }

    // GET: /characters/
    @Operation(summary = "Get all characters")
    @ApiResponse(responseCode = "200", description = "Successful retrieval of all characters")
    @GetMapping("/")
    public ResponseEntity<Collection<CharacterGetDTO>> findAll() {
        return ResponseEntity.ok(
                characterService.findAll()
                        .stream()
                        .map(characterMapper::toCharacterDTO)
                        .collect(Collectors.toList())
        );
    }

    // POST: /characters/
    @Operation(summary = "Create a character")
    @ApiResponse(responseCode = "201", description = "Successful creation of a character")
    @PostMapping("/create")
    public ResponseEntity<CharacterGetDTO> create(@RequestBody CharacterCreateDTO createDTO) {
        return new ResponseEntity<>(
                characterMapper.toCharacterDTO(
                        characterService.add(characterMapper.toCharacter(createDTO))
                )
        , HttpStatus.CREATED);
    }

    // PUT: /characters/{id}
    @Operation(summary = "Update a character")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful update of a character"),
            @ApiResponse(responseCode = "404", description = "Character not found")
    })
    @PutMapping("/{id}")
    public ResponseEntity<CharacterGetDTO> update(@PathVariable Integer id, @RequestBody CharacterCreateDTO updateDTO) {
        if (characterService.findById(id) == null) {
            return ResponseEntity.notFound().build();
        }
        Character character = characterMapper.toCharacter(updateDTO);
        character.setId(id);
        return ResponseEntity.ok(
                characterMapper.toCharacterDTO(
                        characterService.update(character)
                )
        );
    }

    // DELETE: /characters/{id}
    @Operation(summary = "Delete a character")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful deletion of a character"),
            @ApiResponse(responseCode = "404", description = "Character not found")
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<CharacterGetDTO> delete(@PathVariable Integer id) {
        Character character = characterService.findById(id);
        if (character == null) {
            return ResponseEntity.notFound().build();
        }
        characterService.delete(character);
        return ResponseEntity.ok().build();
    }

    // Add characters to a movie
    @Operation(summary = "Add characters to a movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful addition of characters to a movie"),
            @ApiResponse(responseCode = "404", description = "Character not found")
    })
    @PutMapping("/movies/{movieId}/add")
    public ResponseEntity<Collection<CharacterGetDTO>> addCharactersToMovie(@PathVariable Integer movieId, @RequestBody Collection<Integer> characterIds) {
        if (movieService.findById(movieId) == null) {
            return ResponseEntity.notFound().build();
        }

        for(Integer characterId : characterIds) {
            Character character = characterService.findById(characterId);
            if (character == null) {
                return ResponseEntity.notFound().build();
            }
            movieService.addCharacter(movieId, character);
        }

        return findAllByMovie(movieId);
    }


    // Get all the characters of a movie
    // GET: /characters/movie/{id}
    @Operation(summary = "Get all the characters of a movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful retrieval of all characters of a movie"),
            @ApiResponse(responseCode = "404", description = "Movie not found")
    })
    @GetMapping("/movie/{id}")
    public ResponseEntity<Collection<CharacterGetDTO>> findAllByMovie(@PathVariable Integer id) {
        return ResponseEntity.ok(
                characterService.findAllByMovie(id)
                        .stream()
                        .map(characterMapper::toCharacterDTO)
                        .collect(Collectors.toList())
        );
    }

    // Get all characters in a franchise
    // GET: /characters/franchise/{id}
    @Operation(summary = "Get all characters in a franchise")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful retrieval of all characters in a franchise"),
            @ApiResponse(responseCode = "404", description = "Franchise not found")
    })
    @GetMapping("/franchise/{id}")
    public ResponseEntity<Collection<CharacterGetDTO>> findAllByFranchise(@PathVariable Integer id) {
        return ResponseEntity.ok(
                characterService.findAllByFranchise(id)
                        .stream()
                        .map(characterMapper::toCharacterDTO)
                        .collect(Collectors.toList())
        );
    }

}
