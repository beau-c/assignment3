package nl.xvvv.assignment3.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import nl.xvvv.assignment3.dto.franchise.FranchiseCreateDTO;
import nl.xvvv.assignment3.dto.franchise.FranchiseDTO;
import nl.xvvv.assignment3.mappers.franchise.FranchiseMapper;
import nl.xvvv.assignment3.models.Franchise;
import nl.xvvv.assignment3.services.franchise.FranchiseService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "api/v1/franchises")
public class FranchiseController {

    private final FranchiseService franchiseService;
    private final FranchiseMapper franchiseMapper;

    public FranchiseController(final FranchiseService franchiseService, final FranchiseMapper franchiseMapper) {
        this.franchiseService = franchiseService;
        this.franchiseMapper = franchiseMapper;
    }

    // GET: /franchises/{id}
    @Operation(summary = "Get a franchise by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful retrieval of a franchise"),
            @ApiResponse(responseCode = "404", description = "Franchise not found")
    })
    @GetMapping("/{id}")
    public ResponseEntity<FranchiseDTO> findById(@PathVariable Integer id) {
        Franchise franchise = franchiseService.findById(id);
        if (franchise == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(
                franchiseMapper.toFranchiseDTO(franchise)
        );
    }

    // GET: /franchises/
    @Operation(summary = "Get all franchises")
    @ApiResponse(responseCode = "200", description = "Successful retrieval of all franchises")
    @GetMapping("/")
    public ResponseEntity<Collection<FranchiseDTO>> findAll() {
        return ResponseEntity.ok(
                franchiseService.findAll()
                        .stream()
                        .map(franchiseMapper::toFranchiseDTO)
                        .collect(Collectors.toList())
        );
    }

    // POST: /franchises/
    @Operation(summary = "Create a franchise")
    @ApiResponse(responseCode = "201", description = "Successful creation of a franchise")
    @PostMapping("/create")
    public ResponseEntity<FranchiseDTO> create(@RequestBody FranchiseCreateDTO dto) {
        return new ResponseEntity<>(
                franchiseMapper.toFranchiseDTO(franchiseService.add(
                        franchiseMapper.toFranchise(dto)
                ))
        , HttpStatus.CREATED);
    }

    // PUT: /franchises/{id}
    @Operation(summary = "Update a franchise")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful update of a franchise"),
            @ApiResponse(responseCode = "404", description = "Franchise not found")
    })
    @PutMapping("/{id}")
    public ResponseEntity<FranchiseDTO> update(@PathVariable Integer id, @RequestBody FranchiseCreateDTO dto) {
        if (franchiseService.findById(id) == null) {
            return ResponseEntity.notFound().build();
        }
        Franchise franchise = franchiseMapper.toFranchise(dto);
        franchise.setId(id);
        return ResponseEntity.ok(
                franchiseMapper.toFranchiseDTO(
                        franchiseService.update(franchise)
                )
        );
    }

    // DELETE: /franchises/{id}
    @Operation(summary = "Delete a franchise")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful deletion of a franchise"),
            @ApiResponse(responseCode = "404", description = "Franchise not found")
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<FranchiseDTO> delete(@PathVariable Integer id) {
        Franchise franchise = franchiseService.findById(id);
        if (franchise == null) {
            return ResponseEntity.notFound().build();
        }
        franchiseService.delete(franchise);
        return ResponseEntity.ok().build();
    }
}
