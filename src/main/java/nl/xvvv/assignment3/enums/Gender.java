package nl.xvvv.assignment3.enums;

public enum Gender {
    MALE("Male"),
    FEMALE("Female"),
    OTHER("Other"),
    UNKNOWN("Unknown");

    private final String gender;

    Gender(String gender) {
        this.gender = gender;
    }

    public String toString() {
        return this.gender;
    }
}
