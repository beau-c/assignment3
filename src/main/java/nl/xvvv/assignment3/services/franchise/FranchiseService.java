package nl.xvvv.assignment3.services.franchise;

import nl.xvvv.assignment3.models.Franchise;
import nl.xvvv.assignment3.services.CrudService;

public interface FranchiseService extends CrudService<Franchise, Integer> {

}
