package nl.xvvv.assignment3.services.franchise;

import nl.xvvv.assignment3.models.Franchise;
import nl.xvvv.assignment3.repositories.FranchiseRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;

@Service
public class FranchiseServiceImpl implements FranchiseService {

    private final FranchiseRepository franchiseRepository;

    public FranchiseServiceImpl(final FranchiseRepository franchiseRepository) {
        this.franchiseRepository = franchiseRepository;
    }

    @Override
    public Franchise findById(Integer id) {
        return franchiseRepository.findById(id).orElse(null);
    }

    @Override
    public Collection<Franchise> findAll() {
        return franchiseRepository.findAll();
    }

    @Override
    @Transactional
    public Franchise add(Franchise entity) {
        return franchiseRepository.save(entity);
    }

    @Override
    @Transactional
    public Franchise update(Franchise entity) {
        return franchiseRepository.save(entity);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        franchiseRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void delete(Franchise entity) {
        franchiseRepository.delete(entity);
    }
}
