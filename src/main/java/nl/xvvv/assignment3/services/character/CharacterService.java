package nl.xvvv.assignment3.services.character;

import nl.xvvv.assignment3.models.Character;
import nl.xvvv.assignment3.services.CrudService;

import java.util.Collection;

public interface CharacterService extends CrudService<Character, Integer> {
    Collection<Character> findAllByMovie(Integer id);

    Collection<Character> findAllByFranchise(Integer id);
}
