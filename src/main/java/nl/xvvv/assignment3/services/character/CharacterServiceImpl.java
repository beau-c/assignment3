package nl.xvvv.assignment3.services.character;

import nl.xvvv.assignment3.models.Character;
import nl.xvvv.assignment3.repositories.CharacterRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;

@Service
public class CharacterServiceImpl implements CharacterService {

    private final CharacterRepository characterRepository;

    public CharacterServiceImpl(final CharacterRepository characterRepository) {
        this.characterRepository = characterRepository;
    }

    @Override
    public Character findById(Integer id) {
        return characterRepository.findById(id).orElse(null);
    }

    @Override
    public Collection<Character> findAll() {
        return characterRepository.findAll();
    }

    @Override
    @Transactional
    public Character add(Character entity) {
        return characterRepository.save(entity);
    }

    @Override
    @Transactional
    public Character update(Character entity) {
        return characterRepository.save(entity);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        characterRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void delete(Character entity) {
        characterRepository.delete(entity);
    }

    @Override
    public Collection<Character> findAllByMovie(Integer id) {
        return characterRepository.findAllByMovie(id);
    }

    @Override
    public Collection<Character> findAllByFranchise(Integer id) {
        return characterRepository.findAllByFranchise(id);
    }
}
