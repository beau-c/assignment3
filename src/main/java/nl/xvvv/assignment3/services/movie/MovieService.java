package nl.xvvv.assignment3.services.movie;

import nl.xvvv.assignment3.models.Character;
import nl.xvvv.assignment3.models.Movie;
import nl.xvvv.assignment3.services.CrudService;

import java.util.Collection;

public interface MovieService extends CrudService<Movie, Integer> {
    Collection<Movie> findAllByFranchise(Integer id);

    void addCharacter(Integer movieId, Character character);
}
