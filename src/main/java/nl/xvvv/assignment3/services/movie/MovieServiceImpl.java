package nl.xvvv.assignment3.services.movie;

import nl.xvvv.assignment3.models.Character;
import nl.xvvv.assignment3.models.Movie;
import nl.xvvv.assignment3.repositories.MovieRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.Set;

@Service
public class MovieServiceImpl implements MovieService {

    private final MovieRepository movieRepository;

    public MovieServiceImpl(final MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    public Movie findById(Integer id) {
        return movieRepository.findById(id).orElse(null);
    }

    @Override
    public Collection<Movie> findAll() {
        return movieRepository.findAll();
    }

    @Override
    @Transactional
    public Movie add(Movie entity) {
        return this.movieRepository.save(entity);
    }

    @Override
    @Transactional
    public Movie update(Movie entity) {
        return this.movieRepository.save(entity);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        this.movieRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void delete(Movie entity) {
        this.movieRepository.delete(entity);
    }

    @Override
    public void addCharacter(Integer movieId, Character character) {
        Movie movie = findById(movieId);
        Set<Character> movieCharacters = movie.getCharacters();
        movieCharacters.add(character);
        movie.setCharacters(movieCharacters);
        update(movie);
    }

    @Override
    public Collection<Movie> findAllByFranchise(Integer id) {
        return movieRepository.findAllByFranchise(id);
    }
}
