package nl.xvvv.assignment3.services;

import java.util.Collection;

public interface CrudService <T, ID> {

    /***
     * Finds a single entity by its id
     * @param id The id of the entity to find
     * @return The entity with the given id
     */
    T findById(ID id);

    /***
     * Finds all entities
     * @return A collection of all entities
     */
    Collection<T> findAll();

    /***
     * Creates a new entity
     * @param entity The entity to create
     * @return The created entity
     */
    T add(T entity);

    /***
     * Updates the entity with the given id
     * @param entity The entity to update
     * @return The updated entity
     */
    T update(T entity);

    /***
     * Deletes the entity with the given id
     * @param id The id of the entity to delete
     */
    void deleteById(ID id);

    /***
     * Deletes the given entity
     * @param entity The entity to delete
     */
    void delete(T entity);
}