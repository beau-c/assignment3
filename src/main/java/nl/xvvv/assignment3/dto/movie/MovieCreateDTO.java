package nl.xvvv.assignment3.dto.movie;

import lombok.Data;

@Data
public class MovieCreateDTO {
    private String title;
    private String genre;
    private int releaseYear;
    private String director;
    private String pictureURL;
    private String trailerURL;
}
