package nl.xvvv.assignment3.dto.movie;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class MovieDTO {
    private int id;
    private String title;
    private String genre;
    private int releaseYear;
    private String director;
    private String pictureURL;
    private String trailerURL;
    private List<Integer> characters = new ArrayList<>();
}
