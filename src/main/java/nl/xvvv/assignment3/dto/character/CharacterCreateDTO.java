package nl.xvvv.assignment3.dto.character;

import lombok.Data;
import nl.xvvv.assignment3.enums.Gender;

@Data
public class CharacterCreateDTO {
    private String firstName;
    private String lastName;
    private String alias;
    private Gender gender;
    private String pictureURL;
}
