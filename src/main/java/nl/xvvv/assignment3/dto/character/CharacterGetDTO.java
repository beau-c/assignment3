package nl.xvvv.assignment3.dto.character;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class CharacterGetDTO {
    private int id;
    private String fullName;
    private String alias;
    private String gender;
    private String pictureURL;
    private List<Integer> movies = new ArrayList<>();
}
