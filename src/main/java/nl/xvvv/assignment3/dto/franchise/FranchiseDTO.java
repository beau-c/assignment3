package nl.xvvv.assignment3.dto.franchise;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class FranchiseDTO {
    private int id;
    private String name;
    private String description;
    private List<Integer> movies = new ArrayList<>();
}