package nl.xvvv.assignment3.dto.franchise;

import lombok.Data;

@Data
public class FranchiseCreateDTO {
    private String name;
    private String description;
}